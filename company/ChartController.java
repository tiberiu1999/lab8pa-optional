package com.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.company.Database;

public class ChartController{

    int idArtist;
    String NameArtist;
    String CountryArtist;
    public boolean create(Integer albumId, Integer top) {
        Database database=new Database();
        try(Connection conn=database.connent();
            PreparedStatement ps=conn.prepareStatement("INSERT INTO charts VALUES(?,?)")){
            ps.setInt(1,albumId);
            ps.setInt(2, top);
        }
            catch (IllegalAccessException | ClassNotFoundException | InstantiationException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public List<Artist> Artists() {
        List<Artist> List=new ArrayList<>();
        Database database=new Database();
        try(Connection conn=database.connent();
            PreparedStatement ps=conn.prepareStatement(
                    "Select * from artists"


            )){
            ResultSet rs = ps.executeQuery();
            while(rs.next()!=false){
                idArtist=rs.getInt("id");
                NameArtist=rs.getString("name");
                CountryArtist=rs.getString("country");

                Artist artist=new Artist(idArtist,NameArtist,CountryArtist);
                List.add(artist);

            }
            return List;
        }catch(SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException ex){
            ex.printStackTrace();
        }
        return null;
    }

}