package com.company;

public class Chart {
    private Integer id;
    private Integer top;

    public Chart(Integer id, Integer top) {
        this.id = id;
        this.top = top;
    }

    public Chart() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    @Override
    public String toString() {
        return "Chart{" + "album_id=" + id + ", top=" + top + '}';
    }


}