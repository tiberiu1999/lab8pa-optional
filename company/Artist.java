package com.company;

public class Artist {
    private Integer id;
    private String name;
    private String country;

    public Artist(){

    }
    public Artist(Integer id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Artist{" + "id=" + id + ", name=" + name + ", country=" + country + '}'+'\n';
    }

    public void generateRandomArtists()
    {
        ArtistController Controller=new ArtistController();
        this.id=Controller.getId() + 1;
        Controller.increaseId();


        String[] name={"nume1","nume2","nume3","nume4","nume5","nume6","nume7"};

        Random rnd=new Random(name.length);
        this.name=name[rnd.nextInt()];

        String[] countries={"tara1","tara2","tara3","tara4","tara5","tara6","tara7"};
        Random rnd1=new Random(countries.length);
        this.country=countries[rnd1.nextInt()];

        contr.insertArtist(this);
    }
}