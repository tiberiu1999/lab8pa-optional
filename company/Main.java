package com.company;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws Exception {

        Database database = Database.getInstance();

        Connection con = database.connent();

        Statement stmt = con.createStatement();

        ArtistController artistController = new ArtistController(con);
        AlbumController albumController = new AlbumController(con);

        artistController.create("Daniel","USa");
        artistController.findByName("Daniel");

        albumController.create("Clasic",0,1999);
        albumController.findByArtist(1);


        stmt.close();

        ChartController chart=new ChartController();
        chart.create(1,4);
        chart.create(2,3);
        chart.create(3,2);

        database.disconnect(con);

    }
}