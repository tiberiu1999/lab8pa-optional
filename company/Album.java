package com.company;

public class Album {
    private Integer id;
    private String name;
    private Integer artistId;
    private Integer releaseYear;

    public Album(){

    }
    public Album(Integer id, String name, Integer artistId, Integer releaseYear) {
        this.id = id;
        this.name = name;
        this.artistId = artistId;
        this.releaseYear = releaseYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    @Override
    public String toString() {
        return "Album{" + "id=" + id + ", name=" + name + ", artistId=" + artistId + ", releaseYear=" + releaseYear + '}';
    }

    public void generateRandomAlbum()
    {
        AlbumsController contr=new AlbumsController();
        this.id=contr.getId()+1;
        contr.increaseId();

        String[] names={"Album1", "Album2", "Album3", "Album4", "Album5", "Album6", "Album7"};
        Random rnd=new Random(names.length);
        this.nameAlbum=names[rnd.nextInt()];

        ArtistController Controller=new ArtistController();
        int artistNr=Controller.getIdArtist();
        Random rnd1=new Random();
        this.artistId=rnd1.nextInt(artistNr)+1;

        //year
        Random rnd2=new Random();
        this.releaseYear=2000+rnd2.nextInt(70);

    }


}
